<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RegisterController extends AbstractController
{

	/**
	 * @var AuthorizationCheckerInterface
	 */
	private $authorizationChecker;
	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	public function __construct(
		AuthorizationCheckerInterface $authorizationChecker,
		FormFactoryInterface $formFactory
	)
	{
		$this->authorizationChecker = $authorizationChecker;
		$this->formFactory = $formFactory;
	}

	/**
	 * @Route("/register", name="user_register")
	 */
	public function register(UserPasswordEncoderInterface $passwordEncoder, Request $request)
	{
		$user = new User();

		$form = $this->createForm(
			UserType::class,
			$user);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()){
			$password = $passwordEncoder->encodePassword(
				$user,
				$user->getPlainPassword()
			);
			$user->setPassword($password);
			$user->setRoles([$user::ROLE_USER]);

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($user);
			$entityManager->flush();

			$this->redirect('project_index');
		}

		return $this->render('register/register.html.twig',
			[
				'form' => $form->createView()
			]);

	}

}