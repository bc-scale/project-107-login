<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Employee;
use App\Entity\Salary;
use App\Entity\Title;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/employee")
 */
class EmployeeApiController extends AbstractApiController implements BaseApiInterface
{
	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'employee';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;

	}

	/**
	 * @Route("/get", name="api_employee_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Employee::class);
		if ($request->get('empNo') !== null) {
			$entity = $repository->find($request->get('empNo'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getEmployeeJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);
	}

	/**
	 * @Route("/post", name="api_employee_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatEmployee($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Employee created!'
		],
			Response::HTTP_OK);
	}

	/**
	 * @Route("/put", name="api_employee_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');
		$empNo = (int)$formData['empNo'];
		$entity = $this->em->getRepository(Employee::class)->findOneBy(['empNo' => $empNo]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatEmployee($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Employee updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/delete", name="api_employee_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Employee::class);

		try {
			$entity = $repository->find($request->get('empNo'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Employee deleted!'
		],
			Response::HTTP_OK);
	}


	public function getSalaryJsonData (Employee $employee): array
	{
		/** @var Salary $salaries */
		$salaries = $employee->getSalaries();
		$salaryJsonData = [];
		foreach ($salaries as $salary) {
			$salaryJsonData[] = [
				'salary' => $salary->getSalary(),
				'fromDate' => $salary->getFromDate(),
				'toDate' => $salary->getToDate(),
			];
		}
		return $salaryJsonData;
	}

	public function getTitleJsonData (Employee $employee): array
	{
		/** @var Title $titles */
		$titles = $employee->getTitles();
		$titleJsonData = [];
		foreach ($titles as $title) {
			$titleJsonData[] = [
				'title' => $title->getTitle(),
				'fromDate' => $title->getFromDate(),
				'toDate' => $title->getToDate(),
			];
		}
		return $titleJsonData;
	}

	public function getEmployeeJsonData ($employees): array
	{
		$employeeJsonData = [];

		if ($employees instanceof Employee) {
			$employeeJsonData = $this->getEmployeeData($employees, $employeeJsonData);
		} else {
			foreach ($employees as $employee) {
				$employeeJsonData[] = $this->getEmployeeData($employee, $employeeJsonData);
			}
		}

		return $employeeJsonData;
	}

	public function getEmployeeData (Employee $employee, array $employeeJsonData): array
	{
		$salaryJsonData = $this->getSalaryJsonData($employee);
		$titleJsonData = $this->getTitleJsonData($employee);

		$employeeJsonData[] = [
			'empNo' => $employee->getEmpNo(),
			'firstName' => $employee->getFirstName(),
			'lastName' => $employee->getLastName(),
			'birthDate' => $employee->getBirthDate(),
			'hireDate' => $employee->getHireDate(),
			'gender' => $employee->getGender(),
			'titles' => $titleJsonData,
			'salaries' => $salaryJsonData,
		];
		return $employeeJsonData;
	}

	public function creatEmployee (array $formData, Employee $employee = null): void
	{
		if ($employee === null) {
			$employee = new Employee();
		}

		$employee->setEmpNo((int)$formData['empNo']);
		$employee->setHireDate((new \DateTime($formData['hireDate'])));
		$employee->setGender($formData['gender']);
		$employee->setLastName($formData['lastName']);
		$employee->setFirstName($formData['firstName']);
		$employee->setBirthDate((new \DateTime($formData['birthDate'])));

		$this->em->persist($employee);
		$this->em->flush();
	}
}