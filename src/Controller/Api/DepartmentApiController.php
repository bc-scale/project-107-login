<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Department;
use App\Entity\DeptEmp;
use App\Entity\DeptManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/department")
 */
class DepartmentApiController extends AbstractApiController implements BaseApiInterface
{
	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'department';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;
	}

	/**
	 * @Route("/get", name="api_department_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Department::class);
		if ($request->get('deptNo') !== null) {
			$entity = $repository->find($request->get('deptNo'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getDepartmentJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);
	}


	/**
	 * @Route("/post", name="api_department_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatDepartment($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Department created!'
		],
			Response::HTTP_OK);
	}

	/**
	 * @Route("/put", name="api_department_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');
		$entity = $this->em->getRepository(Department::class)->findOneBy(['deptNo' => $formData['deptNo']]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatDepartment($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Department updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/delete", name="api_department_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Department::class);

		try {
			$entity = $repository->find($request->get('deptNo'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Department deleted!'
		],
			Response::HTTP_OK);
	}

	public function getDepartmentJsonData ($departments): array
	{
		$departmentJsonData = [];

		if ($departments instanceof Department) {
			$departmentJsonData = $this->getDepartmentData($departments, $departmentJsonData);
		} else {
			foreach ($departments as $department) {
				$departmentJsonData[] = $this->getDepartmentData($department, $departmentJsonData);
			}
		}

		return $departmentJsonData;
	}

	public function getDepartmentData (Department $department, array $departmentJsonData): array
	{
		$deptEmpJsonData = $this->getDeptEmpJsonData($department);
		$deptManagerJsonData = $this->getDeptManagerJsonData($department);

		$departmentJsonData[] = [
			'deptNo' => $department->getDeptNo(),
			'deptName' => $department->getDeptName(),
			'deptEmp' => $deptEmpJsonData,
			'deptManager' => $deptManagerJsonData,
		];
		return $departmentJsonData;
	}

	public function getDeptEmpJsonData (Department $department): array
	{
		/** @var DeptEmp $deptEmps */
		$deptEmps = $department->getDeptEmps();
		$deptEmpJsonData = [];
		foreach ($deptEmps as $deptEmp) {
			$deptEmpJsonData[] = [
				'deptNo' => $deptEmp->getDeptNo(),
				'fromDate' => $deptEmp->getFromDate(),
				'toDate' => $deptEmp->getToDate(),
			];
		}
		return $deptEmpJsonData;
	}

	private function getDeptManagerJsonData (Department $department): array
	{
		/** @var DeptManager $deptManagers */
		$deptManagers = $department->getDeptManagers();
		$deptManagerJsonData = [];
		foreach ($deptManagers as $deptManager) {
			$deptManagerJsonData[] = [
				'deptNo' => $deptManager->getDeptNo(),
				'fromDate' => $deptManager->getFromDate(),
				'toDate' => $deptManager->getToDate(),
			];
		}
		return $deptManagerJsonData;

	}

	private function creatDepartment ($formData, Department $department = null): void
	{
		if ($department === null) {
			$department = new Department();
		}

		$department->setDeptNo($formData['deptNo']);
		$department->setDeptName($formData['deptName']);

		$this->em->persist($department);
		$this->em->flush();
	}
}