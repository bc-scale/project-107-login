<?php


namespace App\Controller;


use App\Entity\User;

use App\Repository\UserRepository;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

	/**
	 * @var UserPasswordEncoderInterface
	 */
	private $passwordEncoder;
	/**
	 * @var \Twig\Environment
	 */
	private $twig;
	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;
	/**
	 * @var EntityManagerInterface
	 */
	private $entityManager;
	/**
	 * @var RouterInterface
	 */
	private $router;
	/**
	 * @var AuthorizationCheckerInterface
	 */
	private $authorizationChecker;
	/**
	 * @var FlashBagInterface
	 */
	private $flashBag;

	public function __construct(
		UserPasswordEncoderInterface $passwordEncoder,
		\Twig\Environment $twig,
		RouterInterface $router,
		EntityManagerInterface $entityManager,
		FormFactoryInterface $formFactory,
		AuthorizationCheckerInterface $authorizationChecker,
		FlashBagInterface $flashBag
	)
	{
		$this->passwordEncoder = $passwordEncoder;
		$this->twig = $twig;
		$this->formFactory = $formFactory;
		$this->entityManager = $entityManager;
		$this->router = $router;
		$this->authorizationChecker = $authorizationChecker;
		$this->flashBag = $flashBag;
	}

	/**
	 * @Route("/update/{username}", name="user_update")
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function updateAction(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
	{

		$form = $this->formFactory->create(UserType::class, $user);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$this->flashBag->add('notice', 'User updated');

			$password = $passwordEncoder->encodePassword(
				$user,
				$user->getPlainPassword()
			);
			$user->setPassword($password);

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->flush();

			return new RedirectResponse(
				$this->router->generate('project_index')
			);
		}

		return new Response(
			$this->twig->render('user/update.html.twig',
				['form' => $form->createView()])
		);
	}
}